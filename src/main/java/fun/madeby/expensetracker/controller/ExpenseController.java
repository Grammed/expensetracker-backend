package fun.madeby.expensetracker.controller;

import fun.madeby.expensetracker.model.Expense;
import fun.madeby.expensetracker.service.ExpenseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

// This solves the CORS issue for "wildcard" hosts (all):
@CrossOrigin("*")
@RestController
@RequestMapping("api/v1")
public class ExpenseController {

//@Autowired again just illustrating that dependency injection via @Autowired creates this for us.
ExpenseService expenseService;

public ExpenseController(ExpenseService expenseService) {
	this.expenseService = expenseService;
}

@GetMapping("/expenses")
public ResponseEntity<List<Expense>> get() {
	List<Expense> expenses = expenseService.findAll();
	return new ResponseEntity<>(expenses, HttpStatus.OK);
}

@PostMapping("/expenses")
public ResponseEntity<Expense> save(@RequestBody Expense expense) {
	Expense expenseReturned = expenseService.save(expense);
	return new ResponseEntity<>(expenseReturned, HttpStatus.OK);
}

@GetMapping("/expenses/{id}")
public ResponseEntity<Expense> get(@PathVariable("id") Long id) {
	Expense expense = expenseService.findById(id);
	return new ResponseEntity<Expense>(expense, HttpStatus.OK);
}

@DeleteMapping("/expenses/{id}")
public ResponseEntity<String> delete(@PathVariable("id") Long id) {
	expenseService.delete(id);
	return new ResponseEntity<String>("Expense deleted successfully", HttpStatus.OK);
}


}
