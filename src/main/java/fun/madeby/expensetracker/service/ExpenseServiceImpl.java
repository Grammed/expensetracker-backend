package fun.madeby.expensetracker.service;

import fun.madeby.expensetracker.model.Expense;
import fun.madeby.expensetracker.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ExpenseServiceImpl implements ExpenseService {

	@Autowired
	private ExpenseRepository expenseRepository;


@Override
public List<Expense> findAll() {
	return expenseRepository.findAll();
}

@Override
public Expense save(Expense expense) {
	expenseRepository.save(expense);
	return expense;

}

@Override
public Expense findById(Long id) {
	if(expenseRepository.findById(id).isPresent())
		return expenseRepository.findById(id).get();
	return null;
}

@Override
public void delete(Long id) {
	Expense expense = findById(id);
	expenseRepository.delete(expense);
}


}
