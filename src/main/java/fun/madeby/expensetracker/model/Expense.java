package fun.madeby.expensetracker.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
public class Expense {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String expense;
	private String description;
	private BigDecimal amount;

public Expense() {
}

public Expense(String expense, String description, BigDecimal amount) {
	this.expense = expense;
	this.description = description;
	this.amount = amount;
}

public long getId() {
	return id;
}

public void setId(long id) {
	this.id = id;
}

public String getExpense() {
	return expense;
}

public void setExpense(String expense) {
	this.expense = expense;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public BigDecimal getAmount() {
	return amount;
}

public void setAmount(BigDecimal amount) {
	this.amount = amount;
}

@Override
public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	Expense expense = (Expense) o;
	return id == expense.id;
}

@Override
public int hashCode() {
	return Objects.hash(id);
}

@Override
public String toString() {
	return "Expense{" +
	"id=" + id +
	", expense='" + expense + '\'' +
	", description='" + description + '\'' +
	", amount=" + amount +
	'}';
}
}
